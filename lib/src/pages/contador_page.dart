import 'package:flutter/material.dart';

class ContadorPage extends StatefulWidget {
  
  @override
  State<StatefulWidget> createState() => _ContadorPageState();
  
}

class _ContadorPageState extends State<ContadorPage> {
  final _estiloTexto = new TextStyle(fontSize: 25);
  int _conteo = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Statefull'),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Numero de Clicks:', style: _estiloTexto),
            Text(_conteo.toString(), style: _estiloTexto),
          ],
        ),
      ),
      floatingActionButton: _crearBotones(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  Widget _crearBotones() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        FloatingActionButton( child: Icon(Icons.remove), onPressed: _restar),
        SizedBox(width: 10.0),
        FloatingActionButton( child: Icon(Icons.exposure_zero), onPressed: _reset),
        SizedBox(width: 10.0),
        FloatingActionButton( child: Icon(Icons.add), onPressed: _sumar),
      ],
    );
  }

  void _sumar() {
    setState( () => _conteo++ );
  }

  void _restar() {
    setState( () => _conteo-- );
  }

  void _reset() {
    setState( () => _conteo = 0);
  }

}